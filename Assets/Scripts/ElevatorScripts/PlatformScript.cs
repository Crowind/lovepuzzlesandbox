﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformScript : MonoBehaviour{


	SpringJoint spring;

	public float speed=1;
	public GameObject floorHeight;
	public GameObject platformRB;
	public GameObject platformCol;
	public int playersNeeded;

	private Rigidbody rb;
	private float maxDistance;
	private int playerCount = 0;

	public bool switchedOn = false;
	public bool SwitchedOn {
		set { switchedOn = value; }
	}




	// Start is called before the first frame update
	void Awake(){
		spring = platformRB.GetComponent<SpringJoint>();
		maxDistance = platformRB.transform.localPosition.y - floorHeight.transform.localPosition.y;
		rb = spring.gameObject.GetComponent<Rigidbody>();

		transform.localPosition = transform.localPosition - Vector3.up*maxDistance;
		spring.minDistance = maxDistance;
	}

    // Update is called once per frame
    void Update(){

		if ( (!switchedOn || playerCount < playersNeeded ) && spring.minDistance < maxDistance) {

			spring.minDistance += speed * Time.deltaTime;

			if (rb.velocity == Vector3.zero) {

				rb.AddForce(-spring.transform.up);
			}

		}

		if (switchedOn && spring.minDistance > 0 && playerCount >= playersNeeded) {

			spring.minDistance -= speed * Time.deltaTime;

			if (rb.velocity == Vector3.zero) {

				rb.AddForce(spring.transform.up);
			}
			
		}
		
	}
	void OnTriggerEnter(Collider other) {
		if (other.CompareTag("PlayerMesh")) {
			playerCount++;
		}
	}
	void OnTriggerExit(Collider other) {
		if (other.CompareTag("PlayerMesh")) {

			playerCount--;
		}
	}

}
