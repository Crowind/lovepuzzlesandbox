﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElevatorScript : MonoBehaviour{

	private List <ButtonSwitch> switchesArrray;
	private PlatformScript platform;
	

    // Start is called before the first frame update
    void Awake(){
		switchesArrray = new List<ButtonSwitch>();

		foreach (ButtonSwitch sw in GetComponentsInChildren<ButtonSwitch>()) {

			switchesArrray.Add(sw);

		}

		platform = GetComponentInChildren<PlatformScript>();
    }

	// Update is called once per frame
	void Update() {

		bool myActive = true;
		bool partialActive = false;

		foreach (ButtonSwitch sw in switchesArrray) {

			myActive = myActive && sw.Active;
			partialActive = partialActive || sw.Active;
		}
		platform.SwitchedOn = myActive;

	}
}
