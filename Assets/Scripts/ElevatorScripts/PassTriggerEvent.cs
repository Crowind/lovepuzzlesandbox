﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PassTriggerEvent : MonoBehaviour
{
	void onTriggerEnt(Collision collision) {
		
	}

	private void OnTriggerEnter(Collider other) {

		if (other.CompareTag("PlayerMesh")) {

			transform.parent.SendMessage("OnTriggerEnter", other);
		}
	}
	private void OnTriggerExit(Collider other) {

		if (other.CompareTag("PlayerMesh")) {

			transform.parent.SendMessage("OnTriggerExit", other);
		}
	}
}
