﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonLight : MonoBehaviour
{

	public GameObject lamp;


	ButtonSwitch button;
	Color target;
	Color emissiveColor;

	MaterialPropertyBlock prop;
	// Start is called before the first frame update
	void Start() {

		button = GetComponent<ButtonSwitch>();
		emissiveColor = lamp.GetComponent<MeshRenderer>().material.color * 1.5f;

		prop = new MaterialPropertyBlock();
		prop.SetColor("_EmissionColor", Color.black);
		
	}

    // Update is called once per frame
    void Update()
    {
		Color currentColor;

		if ( button.Active) {
			currentColor = emissiveColor;
		}
		else {
			currentColor = Color.black;
		}


		prop.SetColor("_EmissionColor", Color.Lerp(prop.GetColor("_EmissionColor"),currentColor,0.05f ) );

		lamp.GetComponent<MeshRenderer>().SetPropertyBlock(prop);

	}
}
