﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonSwitch : MonoBehaviour
{

	public GameObject button;
	public GameObject threshold;

	private AudioSource [] audioSources;
	private bool active;
	private ParticleSystem sys;
	private Rigidbody rb;
    // Start is called before the first frame update
    void Start(){
		rb = GetComponentInChildren<Rigidbody>();
		sys = GetComponentInChildren<ParticleSystem>();
		audioSources = GetComponents<AudioSource>();
    }

    // Update is called once per frame
    void Update(){

		audioSources[1].pitch= rb.velocity.y/2f;


		if (button.transform.localPosition.y <= threshold.transform.localPosition.y) {

			if (!active && !audioSources[0].isPlaying) {
				audioSources[0].Play();
			}

			active = true;
		}
		else {
			active = false;
		}

		if (active) {
			ParticleSystem.MainModule main = sys.main;
			if (!sys.isPlaying) {
				sys.Play();

				
			}
		}
		if (!active) {
			if (sys.isPlaying) {
				sys.Stop();
			}
		}

		if (rb.velocity == Vector3.zero) {

			rb.AddForce(transform.up);
		}

	}

	public bool Active {
		get { return active; }
	}
	

}
