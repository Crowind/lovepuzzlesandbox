﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SpecificZone : Actor
{
	ParticleSystem pSys;
	BoxCollider[] colls;

	public List<GameObject> safeHearts = null;
    void Awake()
    {
		pSys = GetComponent<ParticleSystem>();
		colls = GetComponentsInChildren<BoxCollider>();

		if (safeHearts != null && safeHearts.Count>0) { 
			foreach (GameObject heart in safeHearts) {
				
				foreach (Collider c in colls) {
					Physics.IgnoreCollision(heart.GetComponent<Collider>(), c);
				}

			}
		}

    }


	private void OnTriggerStay(Collider other) {
		if (other.gameObject.CompareTag("Heart")) {
			Actor actor = other.gameObject.GetComponent<HeartActor>();

			actor.Spawn(null, this);

		}
	}



	public override void Magnetism(Vector3 dir, float targetSpeed, Actor caller) {
		throw new System.NotImplementedException();
	}

	public override void Spawn(GameObject spawnPosistion, Actor caller) {
		throw new System.NotImplementedException();
	}

	public override void Jump(Actor caller) {
		throw new System.NotImplementedException();
	}

	public override void MoveHorizontal(float axisValue,float m ,Actor caller) {
		throw new System.NotImplementedException();
	}
}
