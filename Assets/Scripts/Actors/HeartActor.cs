﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody))]
public class HeartActor : Actor
{

	public GameObject lamp;
	Color emissiveColor;
	Color baseColor;
	Color currentColor;
	MaterialPropertyBlock prop;
	public Light pointLight;

	private bool visible = true;

	private Rigidbody myRb;
	private BoxCollider myCol;
	private MeshRenderer[] myRenderer;

	public GameObject activateParticleObj;
	public GameObject deactivateParticleObj;
	public GameObject myMeshes;
	public GameObject ErrorMesh;
	[Range(0,10)]
	public float errorFeedbackDuration;

	private ParticleSystem activateParticle;
	private ParticleSystem deactivateParticle;

	private Coroutine deactivation;
	private Coroutine error;

	private float cubeMassMax = 100;
	private float cubeMassMin = 1;
	protected Vector3 oldTarget = Vector3.zero;
	private Vector3 extents;

	private AudioSource sound;

	UnityAction action;

	private void Awake() {
		activateParticle = activateParticleObj.GetComponent<ParticleSystem>();
		deactivateParticle = deactivateParticleObj.GetComponent<ParticleSystem>();
		myRb = GetComponent<Rigidbody>();
		myCol = GetComponent<BoxCollider>();
		myRenderer = myMeshes.GetComponentsInChildren<MeshRenderer>();

		extents = GetComponent<Collider>().bounds.extents;
		sound = GetComponent<AudioSource>();


		emissiveColor = lamp.GetComponent<MeshRenderer>().materials[1].GetColor("_EmissionColor");
		baseColor = lamp.GetComponent<MeshRenderer>().materials[1].GetColor("_Color");
		currentColor = baseColor;

		prop = new MaterialPropertyBlock();
		prop.SetColor("_EmissionColor", currentColor);
		prop.SetColor("_Color", currentColor);
	}

	void Update() {

		prop.SetColor("_EmissionColor", Color.Lerp(prop.GetColor("_EmissionColor"), currentColor, 0.5f));
		prop.SetColor("_Color", Color.Lerp(prop.GetColor("_Color"), currentColor, 0.5f));

		pointLight.color = Color.Lerp(prop.GetColor("_Color"), currentColor, 0.5f);

		lamp.GetComponent<MeshRenderer>().SetPropertyBlock(prop,1);

	}

	override public void Magnetism(Vector3 dir, float targetSpeed, Actor caller) {

		if (visible) {
			if (targetSpeed == 0) {
				currentColor = baseColor;
				myRb.useGravity = true;
				myRb.mass = cubeMassMax;
				if (oldTarget != Vector3.zero) {
					myRb.velocity = Vector3.zero;

				}
				oldTarget = Vector3.zero;
				MovePlayerCommand command = new MovePlayerCommand(caller, Vector3.zero, 0, this);
				command.Execute();
			}
			else {
				currentColor = emissiveColor;
				MovePlayerCommand command = new MovePlayerCommand(caller, -Vector3.Project(dir, (oldTarget - myRb.velocity).normalized), targetSpeed, this);
				command.Execute();

				myRb.useGravity = false;
				myRb.mass = cubeMassMin;
				myRb.velocity = targetSpeed * dir;
				oldTarget = myRb.velocity;

			}
		}
		else {
			currentColor = baseColor;
			myRb.useGravity = true;
			myRb.mass = cubeMassMax;

			if (oldTarget != Vector3.zero) {
				myRb.velocity = Vector3.zero;
			}

			oldTarget = Vector3.zero;

			
			MovePlayerCommand command = new MovePlayerCommand(caller, Vector3.zero, 0, this);
			command.Execute();
			
		}
	}

	private void OnDrawGizmos() {

	//	Gizmos.DrawCube(spawnPos.transform.position, extents * 2);
	}

	public override void Spawn(GameObject spawnPos, Actor caller) {

		if (visible ) {
			deactivateParticle.Play();
			activateParticle.Stop();

			if (caller.GetType() == typeof(PlayerController )) {
				sound.clip = ((PlayerController)caller).Info.despawn;
				sound.volume = 0.5f;
				sound.Play();
			}
			foreach (MeshRenderer m in myRenderer) {
				m.enabled = false;

			}
			myCol.enabled = false;
			myRb.isKinematic = true;
			
			action = Deactivate;

			visible = false;

			//deactivation = StartCoroutine(WaitforParticleEnd(deactivateParticle, action ));

			
		}
		else if(spawnPos != null){

			deactivateParticle.Stop();

			bool collision = Physics.CheckBox(spawnPos.transform.position, extents,Quaternion.identity, ~ LayerMask.GetMask("SpecificZone"));

			if (!collision) {

				if (deactivation != null) {
					StopCoroutine(deactivation);
				}
				if (error != null) {
					StopCoroutine(error);
					ErrorMesh.SetActive(false);
				}
				visible = true;
				deactivateParticle.Stop();
				activateParticle.Play();

				sound.clip = ((PlayerController)caller).Info.spawn;
				sound.volume = 0.35f;
				sound.Play();

				foreach (MeshRenderer m in myRenderer) {
					m.enabled = true;

				}
				myCol.enabled = true;
				myRb.isKinematic = false;

				gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
				transform.position = spawnPos.transform.position;
			}
			else {
				if (error != null) {
					StopCoroutine(error);
				}
				sound.clip = ((PlayerController)caller).Info.error;
				sound.volume = 0.5f;
				sound.Play();
				error = StartCoroutine(ErrorFeedback(spawnPos.transform.position, Time.time));
			}
		}
	}

	IEnumerator WaitforParticleEnd(ParticleSystem sys, UnityAction action) {

		while (sys.isPlaying) {

			yield return null;
		}
		action.Invoke();
	}

	void Deactivate() {

		visible = false;
	}

	IEnumerator ErrorFeedback(Vector3 position,float startTime) {
		
		while (Time.time < startTime + errorFeedbackDuration) {


			ErrorMesh.transform.position = position;
			ErrorMesh.SetActive(true);
			yield return null;
		}
		ErrorMesh.SetActive(false);
	}

	public override void Jump(Actor caller) {
		throw new System.NotImplementedException();
	}

	public override void MoveHorizontal(float axisValue,float m ,Actor caller) {
		throw new System.NotImplementedException();
	}
}
