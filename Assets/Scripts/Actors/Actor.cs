﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Actor : MonoBehaviour {

	abstract public void Magnetism(Vector3 dir, float targetSpeed, Actor caller);
	abstract public void Spawn(GameObject spawnPosistion, Actor caller);
	abstract public void Jump(Actor caller);
	abstract public void MoveHorizontal(float axisValue, float multiplier, Actor caller);
}
