﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DebugCommands : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
		if (Input.GetKeyDown(KeyCode.Z)) {
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		}

		if (Input.GetKeyDown(KeyCode.X)) {
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
		}
		if (Input.GetKeyDown(KeyCode.C)) {
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
		}

	}

}
