﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LevelSwap : MonoBehaviour
{
	int count = 0;

    // Update is called once per frame
    void Update()
    {
		if (count == 2) {
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
		}
    }

	private void OnTriggerEnter(Collider other) {
		if (other.CompareTag("Player")) {

			count++;
		}
	}
	private void OnTriggerExit(Collider other) {
		if (other.CompareTag("Player")) {

			count--;
		}
	}
}
