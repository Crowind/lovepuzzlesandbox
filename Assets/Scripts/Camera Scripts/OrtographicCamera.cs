﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrtographicCamera : MonoBehaviour
{
	Camera cam;
	public Material mat;

    // Start is called before the first frame update
    void Start()
    {
		cam = GetComponent<Camera>(); 
    }

    // Update is called once per frame
    void Update()
    {
		if (Input.GetAxis("ShowFullMap") > 0) {

			cam.enabled = true;
		}
		else {
			cam.enabled = false;
		}

    }

	private void OnRenderImage(RenderTexture source, RenderTexture destination) {


		Graphics.Blit(source, destination, mat);
	}
}
