﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class MultipleTargetCamera : MonoBehaviour
{
    public List<Transform> targets;

    public Vector3 offset;
    public float smoothTime = .5f;

    public float minZoom = 40f;
    public float maxZoom = 10f;
    public float zoomLimiter = 50f;

	[Range(0f,1f)]
	public float maxZoomAreaRatio = 0.75f;
	public BoxCollider2D limiter;

    private Vector3 velocity;
    private Camera[] cams;
	private bool inited = false;
	
    void Start()
    {
        cams = GetComponentsInChildren<Camera>();

		zoomLimiter = limiter.bounds.size.x; 

		minZoom =  Mathf.Rad2Deg *  2 * Mathf.Atan(limiter.bounds.extents.y / Mathf.Abs(offset.z));
		maxZoom =  Mathf.Rad2Deg *  2 * Mathf.Atan( Mathf.Sqrt(maxZoomAreaRatio) * limiter.bounds.extents.y / Mathf.Abs(offset.z));
		inited = true;
    }



    void Update()
    {
        if (targets.Count == 0)
            return;
        Zoom();
		
	}
	private void FixedUpdate() {
		Move();
	}

	void Zoom() {
		foreach (var cam in cams) {

			float newZoom = Mathf.Lerp(maxZoom, minZoom, GetGreatestDistance() / zoomLimiter);


			if (Input.GetAxis("ShowFullMap") > 0) {
				cam.fieldOfView = newZoom;
			}
			else {
				cam.fieldOfView = Mathf.MoveTowards(cam.fieldOfView, newZoom, 5f * Time.deltaTime);
			}


		}
    }

    void Move()//offset limitation
    {
        Vector3 middlePoint = GetMiddlePoint();

        Vector3 newPosition = new Vector3(

				Mathf.Clamp((middlePoint + offset).x, limiter.bounds.min.x + getViewBounds().extents.x, limiter.bounds.max.x - getViewBounds().extents.x),
				Mathf.Clamp((middlePoint + offset).y, limiter.bounds.min.y + getViewBounds().extents.y, limiter.bounds.max.y - getViewBounds().extents.y),
				(middlePoint + offset).z
			);

		if (Input.GetAxis("ShowFullMap") > 0) {

			transform.position = newPosition;
		}
		else {

			transform.position = Vector3.SmoothDamp(transform.position, newPosition, ref velocity, smoothTime);
		}
	}

    float GetGreatestDistance()
    {

		if (Input.GetAxis("ShowFullMap")>0) {

			return zoomLimiter;
		}

        var bounds = new Bounds(targets[0].position, Vector3.zero);
        for (int i = 0; i < targets.Count; i++)
        {
            bounds.Encapsulate(targets[i].position);
        }

        return bounds.size.x;
    }

    Vector3 GetMiddlePoint()
    {
        if (targets.Count == 1)
        {
            return targets[0].position;
        }

        var bounds = new Bounds(targets[0].position, Vector3.zero);
        for (int i = 0; i < targets.Count; i++)
        {
            bounds.Encapsulate(targets[i].position);
        }

        return bounds.center;
            
        
    }


	private Bounds getViewBounds() {
		float vertAngle = cams[0].fieldOfView;

		var radAngle = cams[0].fieldOfView * Mathf.Deg2Rad;
		var radHFOV = 2 * Mathf.Atan(Mathf.Tan(radAngle / 2) * cams[0].aspect);

		var horizAngle = Mathf.Rad2Deg * radHFOV;

		Bounds bds = new Bounds(transform.position,
			new Vector3(
				2 * Mathf.Abs(transform.position.z) * Mathf.Tan(Mathf.Deg2Rad * horizAngle * 0.5f),
				2 * Mathf.Abs(transform.position.z) * Mathf.Tan(Mathf.Deg2Rad * vertAngle  * 0.5f), 0));

		return bds;
	}

	void OnDrawGizmos() {
		if (inited) {
			Gizmos.color = Color.red;
			Gizmos.DrawWireCube(new Vector3( transform.position.x,transform.position.y,transform.position.z/2f), new Vector3(getViewBounds().size.x, getViewBounds().size.y,transform.position.z));
		}
	}
}
