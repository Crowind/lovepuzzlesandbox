﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeOut : MonoBehaviour
{
	Image img;
	public float fadeTime = 3f;

	// Start is called before the first frame update
	void Start() {
		img = GetComponent<Image>();

		StartCoroutine(End());
	}


	public IEnumerator End() {



		float start = Time.time;
		Color startColor = img.color;

		while (Time.time < start + fadeTime) {

			yield return null;
			img.color = Color.Lerp(startColor, new Color(startColor.r,startColor.g,startColor.b,0), (Time.time - start) / fadeTime);

		}
		gameObject.SetActive(false);

	}
}
