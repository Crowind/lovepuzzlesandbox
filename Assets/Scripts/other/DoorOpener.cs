﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpener : MonoBehaviour
{

	public GameObject door;
	public AudioSource sound;
	Vector3 open;
	Vector3 closed;
	int count = 0;

	// Update is called once per frame
	private void Awake() {
		open = door.transform.rotation.eulerAngles;
		closed = new Vector3(open.x, open.y + 180, open.z);
		sound = GetComponent<AudioSource>();
	}


	void Update() {
		if (count == 2) {
			bool moving = false;

			if (door.transform.rotation != Quaternion.Euler(closed)) {
				moving = true;
				sound.Play();
			}
			door.transform.rotation = Quaternion.RotateTowards(door.transform.rotation, Quaternion.Euler(closed), 5);
			if (door.transform.rotation == Quaternion.Euler(closed) && moving) {
				sound.Stop();
			}
		}
		else {
			bool moving = false;

			if (door.transform.rotation != Quaternion.Euler(open)) {
				moving = true;
				sound.Play();
			}
			door.transform.rotation = Quaternion.RotateTowards(door.transform.rotation, Quaternion.Euler(open), 5);
			if (door.transform.rotation == Quaternion.Euler(open) && moving) {
				sound.Stop();
			}
		}
	}

	private void OnTriggerEnter(Collider other) {
		if (other.CompareTag("Player")) {

			count++;
		}
	}
	private void OnTriggerExit(Collider other) {
		if (other.CompareTag("Player")) {

			count--;
		}
	}
}
