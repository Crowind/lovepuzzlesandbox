﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum Location { NORTH, SOUTH, WEST, EAST };

[ExecuteInEditMode]
public class ResizeColSZ : MonoBehaviour
{

	public Location loc;
	public GameObject referenceObj;
	private BoxCollider reference;
	private BoxCollider myCol;
    // Start is called before the first frame update
    void Awake()
    {
		myCol = GetComponent<BoxCollider>();
		reference=referenceObj.GetComponent<BoxCollider>();
        
    }

    // Update is called once per frame
    void Update()
    {

		switch (loc) {
			
			case Location.NORTH: {

					myCol.size = new Vector3(reference.size.x, 0, 1);
					myCol.center = new Vector3(0, reference.size.y / 2 + 0.1f, 0);
					break;
				}
			case Location.SOUTH: {

					myCol.size = new Vector3(reference.size.x, 0, 1);
					myCol.center = new Vector3(0, -reference.size.y / 2 - 0.1f, 0);
					break;
				}
			case Location.WEST: {

					myCol.size = new Vector3(0, reference.size.y, 1);
					myCol.center = new Vector3(reference.size.x /2 +0.1f ,0 , 0);
					break;
				}
			case Location.EAST: {

					myCol.size = new Vector3(0, reference.size.y, 1);
					myCol.center = new Vector3(-reference.size.x / 2 - 0.1f, 0, 0);
					break;
				}
		}
		
    }
}
