﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OstController : MonoBehaviour
{
	public static OstController instance;

	public AudioClip[] osts;
	[Range(0, 15)]
	public int intro = 0;
	[Range(0, 15)]
	public int section1 = 2;
	[Range(0, 15)]
	public int section2 = 4;
	[Range(0, 15)]
	public int section3 = 6;
	[Range(0, 15)]
	public int section4 = 8;
	[Range(0, 15)]
	public int finale = 10;

	private int currentScene;
	private int currentSection=1;

	private AudioSourceCrossfade crossfade;

    // Start is called before the first frame update
    void Start(){

		if (OstController.instance == null) {
			OstController.instance = this;
			DontDestroyOnLoad(this.gameObject);
			crossfade = GetComponent<AudioSourceCrossfade>();

			currentScene = SceneManager.GetActiveScene().buildIndex;
			UpdateSection();

			SceneManager.sceneLoaded += OnSceneLoaded;
			

		}
		else {
			GameObject.Destroy(this.gameObject);
		}
    }

    // Update is called once per frame
    void Update()
    {
		



	}

	void OnSceneLoaded(Scene scene, LoadSceneMode mode) {


		currentScene = SceneManager.GetActiveScene().buildIndex;
		UpdateSection();
	}

	void UpdateSection() {

		currentSection = SceneToSection();
		Fade(currentSection);

		Debug.Log("Section: "+currentSection);
	}

	int SceneToSection() {

		Debug.Log("Scene: "+currentScene);
		if (currentScene < section1) {
			return 0;
		}
		if (currentScene < section2) {
			return 1;
		}
		if (currentScene < section3) {
			return 2;
		}
		if (currentScene < section4) {
			return 3;
		}
		if (currentScene < finale) {
			return 4;
		}
		return 5;

	}

	void Fade(int section) {

		crossfade.Play(osts[section]);
	}

}
