﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseScript : MonoBehaviour
{
	public GameObject Background;
	public GameObject Mainmenu;
	public GameObject Selectionmenu;
	public GameObject Options;

	bool on = false;
	bool axisInUse = false;

    // Start is called before the first frame update
    void Start()
    {
		Time.timeScale = 1;
	}

    // Update is called once per frame
    void Update()
    {
		if (Input.GetAxisRaw("Pause") != 0) {
			if (axisInUse == false) {
				axisInUse = true;

				if (on) {
					UnPause();
					
				}
				else {

					Background.SetActive(true);
					Mainmenu.SetActive(true);
					on = true;
					Time.timeScale = 0;
				}


			}
		}
		if (Input.GetAxisRaw("Pause") == 0) {
			axisInUse = false;
		}
	}

	public void UnPause() {
		Background.SetActive(false);
		Mainmenu.SetActive(false);
		Selectionmenu.SetActive(false);
		Options.SetActive(false);
		on = false;
		Time.timeScale = 1;
	}
}
