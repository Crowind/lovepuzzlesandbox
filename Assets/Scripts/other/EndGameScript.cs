﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndGameScript : MonoBehaviour
{
	PlatformScript platform;
	bool activated = false;

	public Image img;


	public float fadeTime = 5;

	// Start is called before the first frame update
	void Start()
    {
		platform = GetComponent<PlatformScript>();
    }

    // Update is called once per frame
    void Update()
    {

		if (platform.switchedOn && !activated) {

			activated = true;
			StartCoroutine(End());
		}
        
    }

	public IEnumerator End() {



		float start = Time.time;
		Color startColor = img.color;

		while (Time.time < start + fadeTime ) {

			yield return null;
			img.color = Color.Lerp(startColor, Color.white, (Time.time - start) / fadeTime);

		}

		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);

	}

}
