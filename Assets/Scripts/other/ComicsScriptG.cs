﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ComicsScriptG : MonoBehaviour
{
	public Sprite[] sprites;
	public float timer = 3;
	public float fadeTime = 1;

	bool advance=true;
	float startTime;
	int index = 0;
	Image img;
	Coroutine fadeCoroutine;

	bool skipAxisInUse = false;
	bool forwardAxisInUse = false;


    // Start is called before the first frame update
    void Start()
    {
		img = GetComponent<Image>();
		img.sprite = sprites[0];
		startTime = Time.time;
		
	}

    // Update is called once per frame
    void Update()
    {


		if (Input.GetAxisRaw("SkipIntro") != 0) {
			if (skipAxisInUse == false) {

				NextScene();
				skipAxisInUse = true;
			}
		}
		if (Input.GetAxisRaw("SkipIntro") == 0) {
			skipAxisInUse = false;
		}


		if (Time.time > startTime + timer && advance ) {

			HardForward();

		}


		if (Input.GetAxisRaw("ForwardComics") != 0) {
			if (forwardAxisInUse == false) {
				
				forwardAxisInUse = true;
				HardForward();
			}
		}
		if (Input.GetAxisRaw("ForwardComics") == 0) {
			
			forwardAxisInUse = false;
		}

	}

	public void HardForward() {

		if (fadeCoroutine != null) {

			StopCoroutine(fadeCoroutine);
		}

		if (index < sprites.Length - 1) {
			img.sprite = sprites[index];
			index++;
			fadeCoroutine = StartCoroutine(SwitchPic());
		}
		else {

			NextScene();
		}
		
	}
	public IEnumerator SwitchPic() {

		advance = false;

		float start = Time.time;

		Color startColor = img.color;


		while (Time.time < start + (fadeTime / 2) * startColor.grayscale) {

			yield return null;
			img.color = Color.Lerp(startColor, Color.black, (Time.time - start) / (fadeTime / 2));

		}


		img.sprite = sprites[index];


		start = Time.time;

		while (Time.time < start + (fadeTime / 2)) {

			yield return null;
			img.color = Color.Lerp(Color.black, Color.white, (Time.time - start) / (fadeTime / 2));

		}
		advance = true;
		startTime = Time.time;

	}
	public IEnumerator FadeAndLoad() {

		
		float start = Time.time;

		Color startColor = img.color;


		while (Time.time < start + (fadeTime / 2) * startColor.grayscale) {

			yield return null;
			img.color = Color.Lerp(startColor, Color.black, (Time.time - start) / (fadeTime / 2));

		}


		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);



		start = Time.time;

		while (Time.time < start + (fadeTime / 2)) {

			yield return null;
			img.color = Color.Lerp(Color.black, Color.white, (Time.time - start) / (fadeTime / 2));

		}
		advance = true;
		startTime = Time.time;

	}

	public void NextScene() {

		StartCoroutine(FadeAndLoad());
	}
}
