﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SpecificZone2 : MonoBehaviour {
	ParticleSystem [] pSys;
	BoxCollider[] colls;

	void Awake() {
		pSys = GetComponentsInChildren<ParticleSystem>();
		colls = GetComponentsInChildren<BoxCollider>();
	}

	void Update() {


		foreach (ParticleSystem item in pSys) {

			ParticleSystem.ShapeModule module = item.shape;
			foreach (var item2 in colls) {
				if (item2.isTrigger) {
					module.scale = colls[0].size;
				}
			}

		}



	}

}
