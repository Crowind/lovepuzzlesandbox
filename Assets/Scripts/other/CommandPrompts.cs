﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandPrompts : MonoBehaviour
{

	public string[] axises;
	private Animator animator;

	public GameObject target;

	public Vector3 delta;

    // Start is called before the first frame update
    void Awake()
    {
		animator = GetComponent<Animator>();
		delta = transform.position - target.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
		transform.position = target.transform.position + delta;

		animator.SetInteger("Anim", 0);

		for (int i = 0; i < axises.Length; i++) {

			if (!(axises[i]).Equals("null")) {

				if (Input.GetAxis(axises[i]) !=0 ) {

					animator.SetInteger("Anim", i+1);

				}
			}

		}

    }
}
