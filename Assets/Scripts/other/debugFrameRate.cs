﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class debugFrameRate : MonoBehaviour
{

	public int framerate = 120;
    // Start is called before the first frame update
    void Awake()
    {
		QualitySettings.vSyncCount = 0;  // VSync must be disabled
		Application.targetFrameRate = framerate;

	}

    // Update is called once per frame
    void Update()
    {
        
    }
}
