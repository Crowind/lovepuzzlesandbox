﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class PromptScript : MonoBehaviour
{
	public string[] axises;
	public float timer;
	public int requiredPlayers=0;
	public int requiredCubes=0;
	public bool onlyOnce = false;
	public int state = 0;


	bool deactivated = false;
	float startTime;
	int currentPlayers=0;
	int currentCubes=0;
	Renderer rend;
	Color target = new Color(0, 0, 0, 0);
	Animator anim;

    // Start is called before the first frame update
    void Start()
    {
		rend = GetComponent<Renderer>();
		anim = GetComponent<Animator>();
		startTime = Time.time;

		rend.material.color = target;

	}

    // Update is called once per frame
    void Update()
    {

		anim.SetInteger("State", state);
		bool b=false;

		foreach (var axis in axises) {
			b = b || (Input.GetAxis(axis) != 0);
		}

		if (!b && (startTime + timer < Time.time) && currentPlayers >= requiredPlayers && currentCubes >= requiredCubes && !deactivated) {
			target = Color.white;
		}
		else if (b) {
			startTime = Time.time;
			target = new Color(0, 0, 0, 0);

			if (onlyOnce) {
				deactivated = true;
			}
		}
		else {
			target = new Color(0, 0, 0, 0);
		}

		rend.material.color = Color.Lerp(rend.material.color, target, 0.1f);

    }
	private void OnTriggerEnter(Collider other) {

		if (other.gameObject.CompareTag("PlayerMesh")) {
			currentPlayers++;
		}
		if (other.gameObject.CompareTag("Heart")) {
			currentCubes++;
		}
	}
	private void OnTriggerExit(Collider other) {
		if (other.gameObject.CompareTag("PlayerMesh")) {
			currentPlayers--;
		}
		if (other.gameObject.CompareTag("Heart")) {
			currentCubes--;
		}
	}
}
