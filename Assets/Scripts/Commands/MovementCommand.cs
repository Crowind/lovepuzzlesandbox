﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementCommand : Command {

	private Actor m_caller;
	private float val;
	private float mult;
	private Actor m_subject;

	public MovementCommand(Actor subject, float axisValue, float multiplier, Actor caller) {
		m_caller = caller;
		val = axisValue;
		mult = multiplier;
		m_subject = subject;
	}

	public override void Execute() {
		m_subject.MoveHorizontal(val, mult, m_caller);
	}
}
