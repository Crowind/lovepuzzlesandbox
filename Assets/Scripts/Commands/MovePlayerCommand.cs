﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayerCommand : Command {

	Vector3 m_dir;
	float m_targetSpeed;
	HeartActor m_caller;
	Actor m_subject;

	public MovePlayerCommand(Actor subject, Vector3 dir, float targetSpeed, HeartActor caller) {
		m_dir = dir;
		m_targetSpeed = targetSpeed;
		m_caller = caller;
		m_subject = subject;
	}

	override public void Execute() {

		m_subject.Magnetism(m_dir, m_targetSpeed,Caller);
	}

	public HeartActor Caller {
		get { return m_caller; }
	}
}
