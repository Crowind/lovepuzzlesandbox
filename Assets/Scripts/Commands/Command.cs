﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class Command 
{
	public abstract void Execute(); 
}
