﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpCommand : Command
{
	PlayerController player;
	Actor m_caller;

	public Actor Caller {
		get { return m_caller; }
	}

	public JumpCommand(PlayerController subject,Actor caller) {
		player = subject;
		m_caller = caller;
	}

	public override void Execute() {

		player.Jump(m_caller);
	}
}
