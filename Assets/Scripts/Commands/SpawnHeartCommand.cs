﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnHeartCommand : Command {

	private GameObject m_spawnPos;
	Actor m_caller;
	Actor m_subject;

	public SpawnHeartCommand(Actor subject, GameObject spawnPos, Actor caller) {

		m_caller = caller;
		m_spawnPos = spawnPos;
		m_subject = subject;
	}

	override public void Execute() {

		m_subject.Spawn(m_spawnPos, Caller);
	}

	public Actor Caller {
		get { return m_caller; }
	}
}
