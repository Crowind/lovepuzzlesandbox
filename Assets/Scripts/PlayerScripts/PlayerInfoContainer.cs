﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
 #if UNITY_EDITOR
 using UnityEditor;
 #endif


public class PlayerInfoContainer : MonoBehaviour {


	private bool inspectorFlag = false;

	public PlayerInfoScriptable scriptableReference;
	public GameObject moveHeart;
	public GameObject spawnHeart;


	public AudioClip error;
	public AudioClip spawn;
	public AudioClip despawn;
	public AudioClip jump;
	public AudioClip land;
	public AudioClip moving;
	public AudioClip airborne;
	public AudioClip magnetism;

	[HideInInspector]
	public int playerNum;
	[HideInInspector]
	public float speed;
	[HideInInspector]
	public float jumpSpeed;
	[HideInInspector]
	public float gravity;
	[HideInInspector]
	public float fastfallMltplr;
	[HideInInspector]
	public float longJumpMltplr;
	[HideInInspector]
	public float airMltplr;

	[HideInInspector]
	public Color particleColor;
	[HideInInspector]
	public bool repulse = true;
	[HideInInspector]
	public float heartSpeed;
	[HideInInspector]
	public float maxRange;


	private ParticleSystem pSys;

	// Start is called before the first frame update
	void Awake() {
		inspectorFlag = true;

		playerNum = scriptableReference.playerNum;
		speed = scriptableReference.speed;
		jumpSpeed = scriptableReference.jumpSpeed;
		gravity = scriptableReference.gravity;
		fastfallMltplr = scriptableReference.fastfallMltplr;
		longJumpMltplr = scriptableReference.longJumpMltplr;
		airMltplr = scriptableReference.airMltplr;
		particleColor = scriptableReference.particleColor;
		heartSpeed = scriptableReference.heartSpeed;
		maxRange = scriptableReference.maxRange;
		repulse = scriptableReference.repulse;

		//pSys = GetComponent<ParticleSystem>();

	}

	// Update is called once per frame
	void Update() {
	}

	void SaveConfiguration() {

	#if UNITY_EDITOR

		PlayerInfoScriptable newConfig = ScriptableObject.CreateInstance<PlayerInfoScriptable>();
		newConfig.playerNum = playerNum;
		newConfig.speed = speed;
		newConfig.jumpSpeed = jumpSpeed;
		newConfig.gravity = gravity;
		newConfig.fastfallMltplr = fastfallMltplr;
		newConfig.longJumpMltplr = longJumpMltplr;
		newConfig.airMltplr = airMltplr;
		newConfig.particleColor = particleColor;
		newConfig.heartSpeed = heartSpeed;
		newConfig.maxRange = maxRange;
		newConfig.repulse = repulse;


		DirectoryInfo d = new DirectoryInfo("Assets/Resources/PlayerSettings/NewConfigurations/");
		int newConfigNumber = DirCount(d);

		AssetDatabase.CreateAsset(newConfig, "Assets/Resources/PlayerSettings/NewConfigurations/newConfig_"+ newConfigNumber +".asset");
		AssetDatabase.SaveAssets();

		EditorUtility.FocusProjectWindow();
		Selection.activeObject = newConfig;

	#endif
	}

	public static int DirCount(DirectoryInfo d) {
		int i = 0;
		FileInfo[] fis = d.GetFiles();
		foreach (FileInfo fi in fis) {
			if (fi.Extension.Contains("asset"))
				i++;
		}
		return i;
	}

#if UNITY_EDITOR
	[CustomEditor(typeof(PlayerInfoContainer))]
	public class RandomScript_Editor : Editor {
		public override void OnInspectorGUI() {
			DrawDefaultInspector(); // for other non-HideInInspector fields

			PlayerInfoContainer script = (PlayerInfoContainer)target;

			if (script.inspectorFlag) // if bool is true, show other fields
			{

				script.playerNum = EditorGUILayout.IntField("Player Number", script.playerNum);
				script.speed = EditorGUILayout.FloatField("Speed", script.speed);
				script.jumpSpeed = EditorGUILayout.FloatField("Jump Speed", script.jumpSpeed);
				script.gravity = EditorGUILayout.FloatField("Gravity", script.gravity);
				script.fastfallMltplr = EditorGUILayout.FloatField("Fast Fall Multiplier", script.fastfallMltplr);
				script.longJumpMltplr = EditorGUILayout.FloatField("Long Press Jump Gravity Multiplier", script.longJumpMltplr);
				script.airMltplr = EditorGUILayout.FloatField("Air Speed Multiplier", script.airMltplr);

				script.particleColor = EditorGUILayout.ColorField("Particle Color", script.particleColor);
				script.repulse = EditorGUILayout.Toggle("Magnetism repulses", script.repulse);

				script.heartSpeed = EditorGUILayout.FloatField("Heart Block Speed", script.heartSpeed);
				script.maxRange = EditorGUILayout.FloatField("Magnetism Max Range", script.maxRange);

				if (GUILayout.Button("Save Configuration")) {
					script.SaveConfiguration();
				}
			}
		}
	}
#endif
}


