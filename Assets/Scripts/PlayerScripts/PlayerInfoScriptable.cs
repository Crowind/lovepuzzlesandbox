﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]

public class PlayerInfoScriptable : ScriptableObject
{
	[Range(1,2)]
	public int playerNum;
	public float speed = 10;
	public float jumpSpeed = 4;
	public float gravity = 9.81f;
	public float fastfallMltplr = 3;
	public float longJumpMltplr = 0.48f;
	public float airMltplr = 1;
	public Color particleColor;
	public float maxRange = 20;
	public float heartSpeed = 5;
	public bool repulse = true;
	public ParticleSystem particleSystem;

}
