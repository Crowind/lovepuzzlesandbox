﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeParticleScript : MonoBehaviour
{
	PlayerInfoContainer info;
	public GameObject particle;

    // Start is called before the first frame update
    void Start()
    {
		info = GetComponent<PlayerInfoContainer>();

	}

    // Update is called once per frame
    void Update(){

		particle.transform.localScale= Vector3.one * info.maxRange;
    }
}
