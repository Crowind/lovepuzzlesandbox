﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatePlayer : MonoBehaviour
{

	public float rotationValue=90;

    private float HorizontalMov;
	private PlayerInfoContainer info;
    
    void Awake()
    {
		info = GetComponent<PlayerInfoContainer>();
    }


    void Update()
    {
        HorizontalMov = Input.GetAxis("Horizontal_P" + info.playerNum.ToString());

        if (HorizontalMov > 0)
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
        else if (HorizontalMov < 0)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
    }
}
