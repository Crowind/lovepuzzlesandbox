﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum State { IDLE = 0, MOVE = 1, AIRBORNE = 2, MAGNETISM = 3};



public interface IPlayerState {
	
	void HandleInput();
	void Update();

}
