﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetismState : IPlayerState {

	PlayerController player;

	public MagnetismState(PlayerController p) {
		player = p;
		foreach (ParticleSystem item in player.PSys) {
			item.Play();
		}
		p.Audiosources[(int)AudioSrcIDs.MAGNETISM].loop = true;
		p.Audiosources[(int)AudioSrcIDs.MAGNETISM].clip = p.Info.magnetism;
		player.Audiosources[(int)AudioSrcIDs.MAGNETISM].volume=0.1f;
		p.Audiosources[(int)AudioSrcIDs.MAGNETISM].Play();


		player.Mat.frictionCombine = PhysicMaterialCombine.Minimum;
		player.Mat.staticFriction = 0;
		player.Mat.dynamicFriction = 0;


	}

	public void HandleInput() {

		player.Mat.frictionCombine = PhysicMaterialCombine.Minimum;
		player.Mat.staticFriction = 0;
		player.Mat.dynamicFriction = 0;


		float magnetism = Input.GetAxis("Fire_P" + player.Info.playerNum.ToString());

		if (magnetism > 0) {
			float repulseInt;

			if (player.Info.repulse) {
				repulseInt = 1;
			}
			else {
				repulseInt = -1;
			}

			float targetSpeed = player.Info.heartSpeed * Input.GetAxis("Fire_P" + player.Info.playerNum.ToString());
			float distance = (player.transform.position - player.MyHeart.transform.position).magnitude;

			if (distance >= player.Info.maxRange) {
				targetSpeed = 0;
				Debug.Log("Out of range");
			}

			//calculate angle

			Vector3 newDir = repulseInt * DirectionBoxBased(player.MyCollider.bounds, player.CubeCollider.bounds.center);


			if (player.NearCube && !player.Info.repulse) {
				targetSpeed = 0;
			}


			player.CommandsQ.Enqueue(new MoveHeartCommand(player.MoveHeartActor, newDir, targetSpeed, player));

		}
		else {
			player.CommandsQ.Enqueue(new MoveHeartCommand(player.MoveHeartActor, Vector3.zero, 0, player));
			float xAxis = Input.GetAxis("Horizontal_P" + player.Info.playerNum.ToString());

			if (!player.Grounded()) {
				player.CommandsQ.Enqueue(new MovementCommand(player, xAxis, player.Info.airMltplr, player));
				player.State = new AirborneState(player);
				foreach (var item in player.PSys) {
					item.Stop();
				}
				player.Audiosources[(int)AudioSrcIDs.MAGNETISM].Stop();

			}
			else if (xAxis == 0) {
				player.State = new IdleState(player);
				foreach (var item in player.PSys) {
					item.Stop();
				}
				player.Audiosources[(int)AudioSrcIDs.MAGNETISM].Stop();
			}
			else if (xAxis != 0) {
				player.CommandsQ.Enqueue(new MovementCommand(player, xAxis, 1, player));
				player.State = new MovingState(player);
				foreach (var item in player.PSys) {
					item.Stop();
				}
				player.Audiosources[(int)AudioSrcIDs.MAGNETISM].Stop();
			}

			float spawn = Input.GetAxis("SpawnP" + player.Info.playerNum.ToString());	

			if (spawn != 0 && player.SpawnAxisInUse == false) {
				player.CommandsQ.Enqueue(new SpawnHeartCommand(player.SpawnHeartActor, player.SpawnPos, player));

				player.SpawnAxisInUse = true;
			}
			if (spawn == 0) {
				player.SpawnAxisInUse = false;
			}


		}





		
	}

	void IPlayerState.Update() {
	}
	private static Vector3 DirectionBoxBased(Bounds bs, Vector3 point) {

		Vector3 ret = new Vector3(
			Mathf.Sign(point.x - bs.max.x) + Mathf.Sign(point.x - bs.min.x),
			Mathf.Sign(point.y - bs.max.y) + Mathf.Sign(point.y - bs.min.y),
			0);
		return ret.normalized;

	}
}
