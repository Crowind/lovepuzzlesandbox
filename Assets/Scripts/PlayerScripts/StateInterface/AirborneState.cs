﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirborneState : IPlayerState { 

	PlayerController player;

	public AirborneState(PlayerController p) {
		player = p;
		player.Mat.frictionCombine = PhysicMaterialCombine.Minimum;
		player.Mat.staticFriction = 0;
		player.Mat.dynamicFriction = 0;

	}

	public void HandleInput() {

		//check grounded

		float xAxis = Input.GetAxis("Horizontal_P" + player.Info.playerNum.ToString());


		if (player.Grounded()) {

			if (xAxis == 0) {
				player.State = new IdleState(player);
			}
			else if (xAxis != 0) {
				player.CommandsQ.Enqueue(new MovementCommand(player, xAxis, 1, player));
				player.State = new MovingState(player);
			}
			if (player.Rb.velocity.y <= 0) {

				player.Audiosources[(int)AudioSrcIDs.LANDING].loop = false;
				player.Audiosources[(int)AudioSrcIDs.LANDING].volume = 0.05f;
				player.Audiosources[(int)AudioSrcIDs.LANDING].PlayOneShot(player.Info.land);
			}
		}
		else if (xAxis != 0) {
			player.CommandsQ.Enqueue(new MovementCommand(player, xAxis, player.Info.airMltplr, player));
		}

		float spawn = Input.GetAxis("SpawnP" + player.Info.playerNum.ToString());

		if (spawn != 0 && player.SpawnAxisInUse == false) {
			player.CommandsQ.Enqueue(new SpawnHeartCommand(player.SpawnHeartActor,  player.SpawnPos, player));

			player.SpawnAxisInUse = true;
		}
		if (spawn == 0) {
			player.SpawnAxisInUse = false;
		}

		float jump = Input.GetAxis("Jump_P" + player.Info.playerNum.ToString());
		if (jump == 0) {
			player.JumpAxisInUse = false;
		}
		//gravity
		if (player.Rb.velocity.y > 0) {
			if (jump > 0) {
				player.GravityMultiplier = player.Info.longJumpMltplr;
			}
			else {
				player.GravityMultiplier = 1;
			}
		}

		if (player.Rb.velocity.y < 0) {
			player.GravityMultiplier=player.Info.fastfallMltplr;
		}
	}

	void IPlayerState.Update() {
	}
}
