﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingState : IPlayerState {

	PlayerController player;

	public MovingState(PlayerController p) {
		player = p;

		player.Audiosources[(int)AudioSrcIDs.MOVEMENT].loop = true;
		player.Audiosources[(int)AudioSrcIDs.MOVEMENT].clip = player.Info.moving;
		player.Audiosources[(int)AudioSrcIDs.MOVEMENT].volume = 0.05f;
		player.Audiosources[(int)AudioSrcIDs.MOVEMENT].Play();

		player.Mat.frictionCombine = PhysicMaterialCombine.Minimum;
		player.Mat.staticFriction = 0;
		player.Mat.dynamicFriction = 0;
	}

	public void HandleInput() {


		float xAxis = Input.GetAxis("Horizontal_P" + player.Info.playerNum.ToString());

		player.Audiosources[(int)AudioSrcIDs.MOVEMENT].pitch =Mathf.Abs( xAxis );


		//check grounded

		if (!player.Grounded()) {
			player.CommandsQ.Enqueue(new MovementCommand(player, xAxis, player.Info.airMltplr, player));
			player.State = new AirborneState(player);

			player.Audiosources[(int)AudioSrcIDs.MOVEMENT].Stop();
		}

		else {      //check axis values


			player.CommandsQ.Enqueue(new MovementCommand(player , xAxis, 1, player));

			float magnetism = Input.GetAxis("Fire_P" + player.Info.playerNum.ToString());
			float jump = Input.GetAxis("Jump_P" + player.Info.playerNum.ToString());



			if (jump > 0 && !player.JumpAxisInUse) {
				player.CommandsQ.Enqueue(new JumpCommand(player, player));
				player.JumpAxisInUse = true;
				player.State = new AirborneState(player);

				player.Audiosources[(int)AudioSrcIDs.MOVEMENT].Stop();

				player.Audiosources[(int)AudioSrcIDs.JUMP].loop = false;
				player.Audiosources[(int)AudioSrcIDs.JUMP].clip = player.Info.jump;
				player.Audiosources[(int)AudioSrcIDs.JUMP].volume = 0.05f;
				player.Audiosources[(int)AudioSrcIDs.JUMP].Play();
			}
			else if (magnetism > 0) {

				player.Audiosources[(int)AudioSrcIDs.MOVEMENT].Stop();
				player.State = new MagnetismState(player);
			}
			else if (xAxis == 0) {

				player.Audiosources[(int)AudioSrcIDs.MOVEMENT].Stop();
				player.State = new IdleState(player);
			}
			if (jump == 0) {
				player.JumpAxisInUse = false;
			}
		}

		float spawn = Input.GetAxis("SpawnP" + player.Info.playerNum.ToString());

		if (spawn != 0 && player.SpawnAxisInUse == false) {
			player.CommandsQ.Enqueue(new SpawnHeartCommand(player.SpawnHeartActor, player.SpawnPos, player));

			player.SpawnAxisInUse = true;
		}
		if (spawn == 0) {
			player.SpawnAxisInUse = false;
		}
		
	}

	void IPlayerState.Update() {
	}
}
