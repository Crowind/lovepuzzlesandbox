﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleState : IPlayerState
{

	PlayerController player;

	public IdleState(PlayerController p) {
		player = p;
	}

	public void HandleInput() {

		//check grounded
		player.Mat.frictionCombine = PhysicMaterialCombine.Maximum;
		player.Mat.dynamicFriction = 10000;
		player.Mat.staticFriction = 10000;
		
		float jump = Input.GetAxis("Jump_P" + player.Info.playerNum.ToString());	

		if (!player.Grounded()) {
			player.State = new AirborneState(player);
		}

		else {		//check axis values
			float magnetism = Input.GetAxis("Fire_P" + player.Info.playerNum.ToString());
			
			float xAxis = Input.GetAxis("Horizontal_P" + player.Info.playerNum.ToString());

			if (magnetism != 0 || (jump != 0) || xAxis != 0) {
				if (jump > 0 && !player.JumpAxisInUse) {
					player.JumpAxisInUse = true;
					player.CommandsQ.Enqueue(new JumpCommand(player, player));
					player.JumpAxisInUse = true;
					player.State = new AirborneState(player);
					player.Audiosources[(int)AudioSrcIDs.JUMP].loop = false;
					player.Audiosources[(int)AudioSrcIDs.JUMP].clip = player.Info.jump;
					player.Audiosources[(int)AudioSrcIDs.JUMP].volume = 0.05f;
					player.Audiosources[(int)AudioSrcIDs.JUMP].Play();
				}
				else if (magnetism > 0) {
					player.State = new MagnetismState(player);
				}
				else if (xAxis != 0) {
					player.State = new MovingState(player);
					player.CommandsQ.Enqueue(new MovementCommand(player, xAxis, 1, player));

				}
				if (jump == 0) {
					player.JumpAxisInUse = false;
				}
			}
			if (xAxis== 0) {
				player.CommandsQ.Enqueue(new MovementCommand(player, 0, 1, player));
			}
		}
		float spawn = Input.GetAxis("SpawnP" + player.Info.playerNum.ToString());

		if (spawn != 0 && player.SpawnAxisInUse == false) {
			player.CommandsQ.Enqueue(new SpawnHeartCommand(player.SpawnHeartActor, player.SpawnPos, player));

			player.SpawnAxisInUse = true;
		}
		if (spawn == 0) {
			player.SpawnAxisInUse = false;
		}
		if (jump == 0) {
			player.JumpAxisInUse = false;
		}
	}
	   
	void IPlayerState.Update() {
	}
}
