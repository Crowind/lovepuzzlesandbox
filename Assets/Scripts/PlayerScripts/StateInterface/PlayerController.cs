﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AudioSrcIDs {MAGNETISM,MOVEMENT,JUMP,LANDING }

[RequireComponent(typeof(Rigidbody))]

public class PlayerController : Actor
{
	public GameObject meshObj;
	private Queue<Command> commandsQ;
	private Rigidbody rb;
	private PlayerInfoContainer info;
	private IPlayerState state;
	[SerializeField]
	private GameObject spawnPos;
	

	private GameObject myHeart;
	private HeartActor moveHeartActor;
	private HeartActor spawnHeartActor;
	private ParticleSystem[] pSys;
	private BoxCollider cubeCollider;
	private BoxCollider myCollider;
	private bool jumpAxisInUse;
	private bool spawnAxisInUse;
	private bool nearCube;
	private float gravityMultiplier = 1;

	private AudioSource[] audiosources;


	private PhysicMaterial mat;

	void Awake() {
		Mat = meshObj.GetComponent<Collider>().material;
		info = gameObject.GetComponent<PlayerInfoContainer>();
		rb = gameObject.GetComponent<Rigidbody>();
		Rb.useGravity = false;
		commandsQ = new Queue<Command>();
		State = new IdleState(this);
		myHeart = info.moveHeart;
		cubeCollider = myHeart.GetComponent<BoxCollider>();
		myCollider = GetComponent<BoxCollider>();
		spawnHeartActor = info.spawnHeart.GetComponent<HeartActor>();
		moveHeartActor = myHeart.GetComponent<HeartActor>();
		pSys = GetComponentsInChildren<ParticleSystem>();
		audiosources = GetComponents<AudioSource>();
		jumpAxisInUse = false;
		spawnAxisInUse = false;
		nearCube = false;

	}


	void Update() {

		if (!cubeCollider.enabled) {
			nearCube = false;
		}

		State.Update();
		State.HandleInput();
	}

	void FixedUpdate() {


		while (CommandsQ.Count > 0) {

			CommandsQ.Dequeue().Execute();
		}

		rb.AddForce(0, -info.gravity * gravityMultiplier, 0);
		
	}

	#region Properties
	public IPlayerState State {
		get { return state; }
		set { state = value; }
	}
	public PlayerInfoContainer Info {
		get {return info;}
	}
	public Rigidbody Rb {
		get {return rb;}
	}
	public Queue<Command> CommandsQ {
		get {return commandsQ;}
	}
	public GameObject SpawnPos {
		get {return spawnPos;}
	}

	public GameObject MyHeart {
		get {return myHeart;}
	}

	public HeartActor SpawnHeartActor {
		get {return spawnHeartActor;}
	}

	public ParticleSystem[] PSys {
		get {return pSys;}
	}

	public BoxCollider CubeCollider {
		get {return cubeCollider;}
	}

	public BoxCollider MyCollider {
		get {return myCollider;}
	}

	public bool JumpAxisInUse {
		get {return jumpAxisInUse;}
		set {jumpAxisInUse = value;}
	}

	public bool SpawnAxisInUse {
		get {return spawnAxisInUse;}
		set {spawnAxisInUse = value;}
	}

	public bool NearCube {
		get {return nearCube;}
	}

	public HeartActor MoveHeartActor {
		get {return moveHeartActor;}
	}

	public float GravityMultiplier {
		get {
			return gravityMultiplier;
		}

		set {
			gravityMultiplier = value;
		}
	}

	public AudioSource[] Audiosources {
		get {return audiosources;}
	}

	public PhysicMaterial Mat {
		get {
			return mat;
		}

		set {
			mat = value;
		}
	}

	#endregion

	public override void Magnetism(Vector3 dir, float targetSpeed, Actor caller) {
		rb.velocity = dir * targetSpeed;
	}

	public override void Spawn(GameObject spawnPosistion, Actor caller) {
	
	}

	public override void Jump(Actor caller) {
		Rb.velocity = Rb.velocity.x * Vector3.right + (Info.jumpSpeed * Vector3.up);
	}

	public override void MoveHorizontal(float axisValue, float multiplier, Actor caller) {

		float speedX = multiplier * info.speed * axisValue;
		float speedDifference = Mathf.Abs(speedX - Rb.velocity.x);
		float newXspeed = (Rb.velocity.x * speedDifference * 2 + multiplier * info.speed * axisValue) / (1 + speedDifference * 2);

		Rb.velocity = (newXspeed * Vector3.right) + (Rb.velocity.y * Vector3.up);
	}





	private void OnTriggerEnter(Collider other) {
		if (other.gameObject == myHeart) {
			nearCube = true;
		}
	}

	private void OnTriggerStay(Collider other) {
		if (other.gameObject == myHeart) {
			nearCube = true;
		}
	}

	private void OnTriggerExit(Collider other) {
		if (other.gameObject == myHeart) {
			nearCube = false;
		}
	}

	public bool Grounded() {

		bool grounded1 = Physics.Linecast(
			transform.position - Vector3.down * 0.2f,
			transform.position + Vector3.down * 0.1f,
			(1 << LayerMask.NameToLayer("Environment")) |(1 << LayerMask.NameToLayer("Button")));
		bool grounded2 = Physics.Linecast(
			transform.position - Vector3.down * 0.2f + Vector3.right * 0.15f,
			transform.position + Vector3.down * 0.1f + Vector3.right * 0.15f,
			(1 << LayerMask.NameToLayer("Environment")) | (1 << LayerMask.NameToLayer("Button")));
		bool grounded3 = Physics.Linecast(
			transform.position - Vector3.down * 0.2f - Vector3.right * 0.15f,
			transform.position + Vector3.down * 0.1f - Vector3.right * 0.15f,
			(1 << LayerMask.NameToLayer("Environment")) | (1 << LayerMask.NameToLayer("Button")));
		bool grounded4 = Physics.Linecast(
	   transform.position - Vector3.down * 0.2f + Vector3.right * 0.3f,
	   transform.position + Vector3.down * 0.1f + Vector3.right * 0.3f,
	   (1 << LayerMask.NameToLayer("Environment")) | (1 << LayerMask.NameToLayer("Button")));
		bool grounded5 = Physics.Linecast(
			transform.position - Vector3.down * 0.2f - Vector3.right * 0.3f,
			transform.position + Vector3.down * 0.1f - Vector3.right * 0.3f,
			(1 << LayerMask.NameToLayer("Environment")) | (1 << LayerMask.NameToLayer("Button")));


		Debug.DrawLine(transform.position - Vector3.down * 0.2f, transform.position + Vector3.down * 0.1f, Color.red, 2f);
		Debug.DrawLine(transform.position - Vector3.down * 0.2f + Vector3.right * 0.15f, transform.position + Vector3.down * 0.1f + Vector3.right * 0.15f, Color.red, 2f);
		Debug.DrawLine(transform.position - Vector3.down * 0.2f + Vector3.right * 0.3f, transform.position + Vector3.down * 0.1f + Vector3.right * 0.3f, Color.red, 2f);
		Debug.DrawLine(transform.position - Vector3.down * 0.2f - Vector3.right * 0.15f, transform.position + Vector3.down * 0.1f - Vector3.right * 0.15f, Color.red, 2f);
		Debug.DrawLine(transform.position - Vector3.down * 0.2f - Vector3.right * 0.3f, transform.position + Vector3.down * 0.1f - Vector3.right * 0.3f, Color.red, 2f);

		return (grounded1 || grounded2 || grounded3 || grounded4 || grounded5);
	}


}
